export const organization = process.env.INFLUX_DB_ORGANIZATION;
export const bucket = process.env.INFLUX_DB_BUCKET;
export const token = process.env.INFLUX_DB_TOKEN;