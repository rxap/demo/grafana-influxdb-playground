import client from './init-client';
import { bucket, organization } from './config';
import {Point} from '@influxdata/influxdb-client';


const writeApi = client.getWriteApi(organization, bucket)
writeApi.useDefaultTags({host: 'host1'})

const point = new Point('mem').floatField('used_percent', 23.43234543)
writeApi.writePoint(point)

writeApi
    .close()
    .then(() => {
        console.log('FINISHED')
    })
    .catch(e => {
        console.error(e)
        console.log('Finished ERROR')
    })