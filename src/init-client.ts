import {InfluxDB} from '@influxdata/influxdb-client';
import { token } from './config';

const client = new InfluxDB({url: 'http://localhost:8086', token: token})

export default client;