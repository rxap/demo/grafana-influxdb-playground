# Grapana + InfluxDB Testing

## Setup

To start the playground run `yarn start`. 

Open Grafana: [http://localhost:3000](http://localhost:3000). The default username and password is `admin`.

Open InfluxDB: [http://localhost:8086](http://localhost:8086). The default username is `admin` and teh default password is `admin`. The admin token is set to `adminadminadmin`.

## Playground

In the file `src/write-data.ts` an example datapoint is written to influxDB. With the command `yarn write-data` the example can be executed.

In the file `src/execute-flux-query.ts` an example flux query is executed. With the command `yarn execute-flux-query` the example can be executed.

### Utility commands

*clear all volumes*: `docker volume rm $(docker volume ls -q)`