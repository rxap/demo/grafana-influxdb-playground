import client from './init-client';
import { bucket, organization } from './config';
import {Point} from '@influxdata/influxdb-client';

const queryApi = client.getQueryApi(organization)

const query = `from(bucket: "${bucket}") |> range(start: -1h)`;
queryApi.queryRows(query, {
  next(row, tableMeta) {
    const o = tableMeta.toObject(row)
    console.log(`${o._time} ${o._measurement}: ${o._field}=${o._value}`)
  },
  error(error) {
    console.error(error)
    console.log('Finished ERROR')
  },
  complete() {
    console.log('Finished SUCCESS')
  },
})
